# vim:set ts=4 sw=4 et nowrap syntax=python ff=unix:
#
# Copyright 2020 Mark Crewson <mark@crewson.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import aiohttp

from packmaker.download import RetryableHttpClient

##############################################################################


class Webapp:
    def __init__(self):
        self.counter = 0

        app = aiohttp.web.Application()
        app.router.add_get('/ping', self.ping_handler)
        app.router.add_get('/internal_error', self.internal_error_handler)
        app.router.add_get('/not_found_error', self.not_found_error_handler)
        app.router.add_get('/sometimes_error', self.sometimes_error)

        self.app = app

    async def ping_handler(self, request):
        self.counter += 1
        return aiohttp.web.Response(text='Ok!', status=200)

    async def internal_error_handler(self, request):
        self.counter += 1
        return aiohttp.web.HTTPInternalServerError()

    async def not_found_error_handler(self, request):
        self.counter += 1
        return aiohttp.web.HTTPNotFound()

    async def sometimes_error(self, request):
        self.counter += 1
        if self.counter == 3:
            return aiohttp.web.Response(text='Ok!', status=200)
        return aiohttp.web.HTTPInternalServerError()

    def get_app(self):
        return self.app


##############################################################################

class TestRetryableHttpClient(object):

    async def test_ok(self, aiohttp_client, loop):
        testapp = Webapp()
        client = await aiohttp_client(testapp.get_app())
        retry_client = RetryableHttpClient()
        retry_client.client = client

        async with retry_client.get('/ping') as response:
            text = await response.text()
            assert response.status == 200
            assert text == 'Ok!'
            assert testapp.counter == 1

        await retry_client.close()
        await client.close()

    async def test_ok_with_context(self, aiohttp_client, loop):
        testapp = Webapp()
        client = await aiohttp_client(testapp.get_app())

        async with RetryableHttpClient() as retry_client:
            retry_client.client = client
            async with retry_client.get('/ping') as response:
                text = await response.text()
                assert response.status == 200
                assert text == 'Ok!'
                assert testapp.counter == 1

        await client.close()

    async def test_internal_error(self, aiohttp_client, loop):
        testapp = Webapp()
        client = await aiohttp_client(testapp.get_app())
        retry_client = RetryableHttpClient(attempts=5)
        retry_client.client = client

        async with retry_client.get('/internal_error') as response:
            assert response.status == 500
            assert testapp.counter == 5

        await retry_client.close()
        await client.close()

    async def test_not_found_error(self, aiohttp_client, loop):
        testapp = Webapp()
        client = await aiohttp_client(testapp.get_app())
        retry_client = RetryableHttpClient(attempts=5, statuses=[404])
        retry_client.client = client

        async with retry_client.get('/not_found_error') as response:
            assert response.status == 404
            assert testapp.counter == 5

        await retry_client.close()
        await client.close()

    async def test_sometimes_error(self, aiohttp_client, loop):
        testapp = Webapp()
        client = await aiohttp_client(testapp.get_app())
        retry_client = RetryableHttpClient(attempts=5)
        retry_client.client = client

        async with retry_client.get('/sometimes_error') as response:
            text = await response.text()
            assert response.status == 200
            assert text == 'Ok!'
            assert testapp.counter == 3

        await retry_client.close()
        await client.close()

    async def test_sometimes_error_with_raise_for_status(self, aiohttp_client, loop):
        testapp = Webapp()
        client = await aiohttp_client(testapp.get_app(), raise_for_status=True)
        retry_client = RetryableHttpClient(attempts=5, exceptions=[aiohttp.client_exceptions.ClientResponseError])
        retry_client.client = client

        async with retry_client.get('/sometimes_error') as response:
            text = await response.text()
            assert response.status == 200
            assert text == 'Ok!'
            assert testapp.counter == 3

        await retry_client.close()
        await client.close()


##############################################################################
##############################################################################
##############################################################################
##############################################################################
# THE END
