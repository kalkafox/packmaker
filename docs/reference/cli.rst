Command-Line Interface
======================

.. only:: man

    SYNOPSIS
    --------

    | **packmaker** [*args*...] *command* [*args*...]
    | **packmaker help** *command*

.. only:: html

    **packmaker** is the command-line interface to Packmaker.

    You invoke packmaker by specifying a *command*, like so::

        packmaker COMMAND [ARGS...]

    The rest of this document describes the available
    commands. If you ever need a quick list of what's available, just
    type ``packmaker -h`` or ``packmaker help COMMAND`` for help with a specific
    command.



Commands
--------

.. only:: html

    Here are the built-in commands available in packmaker:

    .. contents::
        :local:
        :depth: 1

    Also be sure to see the :ref:`global flags <global-flags>`.

.. _build-curseforge-cmd:

build-curseforge
````````````````
::

    packmaker build-curseforge [-h] [--build-dir BUILD_DIR]
                                      [--release-dir RELEASE_DIR]
                                      [--cache-dir CACHE_DIR]
                                      [--release-format {zip,tgz}]
                                      [lockfile [lockfile ...]]
    
    Build a curseforge compatible modpack.
    
    positional arguments:
      lockfile              modpack lock file
    
    optional arguments:
      -h, --help            show this help message and exit
      --build-dir BUILD_DIR, -b BUILD_DIR
                            base directory for build artifacts
      --release-dir RELEASE_DIR, -r RELEASE_DIR
                            base directory for release artifacts
      --cache-dir CACHE_DIR
                            base directory for cached artifacts
      --release-format {zip,tgz}
                            archive format for release package

Describe cmd here

Optional command flags:


.. _build-local-cmd:

build-local
```````````
::

    packmaker build-local [-h] [--build-dir BUILD_DIR]
                                 [--release-dir RELEASE_DIR]
                                 [--cache-dir CACHE_DIR]
                                 [--release-format {zip,tgz}]
                                 [lockfile [lockfile ...]]
    
    Build a local installation
    
    positional arguments:
      lockfile              modpack lock file
    
    optional arguments:
      -h, --help            show this help message and exit
      --build-dir BUILD_DIR, -b BUILD_DIR
                            base directory for build artifacts
      --release-dir RELEASE_DIR, -r RELEASE_DIR
                            base directory for release artifacts
      --cache-dir CACHE_DIR
                            base directory for cached artifacts
      --release-format {zip,tgz}
                            archive format for release package


Describe cmd here

Optional command flags:


.. _build-routhio-cmd:

build-routhio
`````````````
::

    packmaker build-routhio [-h] [--build-dir BUILD_DIR]
                                 [--release-dir RELEASE_DIR]
                                 [--cache-dir CACHE_DIR]
                                 [--release-format {zip,tgz}]
                                 [lockfile [lockfile ...]]

    Build a Routh.io compatible modpack.

    positional arguments:
      lockfile              modpack lock file

    optional arguments:
      -h, --help            show this help message and exit
      --build-dir BUILD_DIR, -b BUILD_DIR
                            base directory for build artifacts
      --release-dir RELEASE_DIR, -r RELEASE_DIR
                            base directory for release artifacts
      --cache-dir CACHE_DIR
                            base directory for cached artifacts
      --release-format {zip,tgz}
                            archive format for release package

Describe cmd here

Optional command flags:


.. _build-server-cmd:

build-server
````````````
::

    packmaker build-server [-h] [--build-dir BUILD_DIR]
                                  [--release-dir RELEASE_DIR]
                                  [--cache-dir CACHE_DIR]
                                  [--release-format {zip,tgz}]
                                  [lockfile [lockfile ...]]
    
    Build a server modpack.
    
    positional arguments:
      lockfile              modpack lock file
    
    optional arguments:
      -h, --help            show this help message and exit
      --build-dir BUILD_DIR, -b BUILD_DIR
                            base directory for build artifacts
      --release-dir RELEASE_DIR, -r RELEASE_DIR
                            base directory for release artifacts
      --cache-dir CACHE_DIR
                            base directory for cached artifacts
      --release-format {zip,tgz}
                            archive format for release package


Describe cmd here

Optional command flags:


.. _findupdates-cmd:

convert
```````

::

    packmaker convert [-h] manifest [packdef]

    Convert a curse/twitch modpack manifest json file to a packmaker yaml packdef file

    positional arguments:
      manifest    manifest json file
      packdef     output packdef file

    optional arguments:
      -h, --help  show this help message and exit

Describe cmd here

Optional command flags:


.. _findupdates-cmd:

findupdates
```````````
::

    packmaker findupdates [-h] [-f {csv,json,table,value,yaml}] [-c COLUMNS]
                                 [--max-width <integer>] [--fit-width]
                                 [--print-empty]
                                 [--quote {all,minimal,none,nonnumeric}]
                                 [--noindent] [--sort-column SORT_COLUMN]
                                 [lockfile [lockfile ...]]
    
    Search curseforge for newer/updated versions of addons.
    
    positional arguments:
      lockfile              modpack lock file
    
    optional arguments:
      -h, --help            show this help message and exit
    
    output formatter:
      output formatter options
    
      -f {csv,json,table,value,yaml}, --format {csv,json,table,value,yaml}
                            the output format, defaults to table
      -c COLUMNS, --column COLUMNS
                            specify the column(s) to include, can be repeated
      --sort-column SORT_COLUMN
                            specify the column(s) to sort the data (columns
                            specified first have a priority, non-existing columns
                            are ignored), can be repeated
    
    Table formatter:
      --max-width <integer>
                            maximum display width, <1 to disable. You cal also use
                            the MAX_DISPLAY_WIDTH environment variable, but the
                            parameter takes precedence.
      --fit-width           Fit the table to the display width. Implied if --max-
                            width greater than 0. Set the environment variable
                            FIT_WIDTH=1 to always enable
      --print-empty         Print empty table iof there is no data to show.
    
    CSV formatter:
      --quote {all,minimal,none,nonnumeric}
                            when to include quotes, default to nonnumeric
    
    json formatter:
      --noindent            whether to disable indenting the JSON

Describe cmd here

Optional command flags:


.. _info-cmd:

info
````
::

    packmaker info [-h] [-f {json,shell,table,value,yaml}] [-c COLUMNS]
                          [--max-width <integer>] [--fit-width] [--print-empty]
                          [--noindent] [--prefix PREFIX]
                          [lockfile [lockfile ...]]

    Display information about the modpack.

    positional arguments:
      lockfile              modpack lock file

    optional arguments:
      -h, --help            show this help message and exit

    output formatter:
      output formatter options

      -f {json,shell,table,value,yaml}, --format {json,shell,table,value,yaml}
                            the output format, defaults to table
      -c COLUMNS, --column COLUMNS
                            specify the column(s) to include, can be repeated

    Table formatter:
      --max-width <integer>
                            maximum display width, <1 to disable. You cal also use
                            the MAX_DISPLAY_WIDTH environment variable, but the
                            parameter takes precedence.
      --fit-width           Fit the table to the display width. Implied if --max-
                            width greater than 0. Set the environment variable
                            FIT_WIDTH=1 to always enable
      --print-empty         Print empty table iof there is no data to show.

    json formatter:
      --noindent            whether to disable indenting the JSON

    shell formatter:
      a format a UNIX shell can parse (variable="value")

      --prefix PREFIX       add a prefix to all variable names

Describe cmd here

Optional command flags:

.. _launch-cmd:

launch
``````
::

    packmaker launch [-h] [--build-dir BUILD_DIR]
                            [--release-dir RELEASE_DIR] [--cache-dir CACHE_DIR]
                            [--release-format {zip,tgz}]
                            [lockfile [lockfile ...]]

    Launch a local installation

    positional arguments:
      lockfile              modpack lock file

    optional arguments:
      -h, --help            show this help message and exit
      --build-dir BUILD_DIR, -b BUILD_DIR
                            base directory for build artifacts
      --release-dir RELEASE_DIR, -r RELEASE_DIR
                            base directory for release artifacts
      --cache-dir CACHE_DIR
                            base directory for cached artifacts
      --release-format {zip,tgz}
                            archive format for release package

Describe cmd here

Optional command flags:


.. _lock-cmd:

lock
````
::

    packmaker lock [-h] [packdef [packdef ...]]

    Lock the modpack. Find mod download urls, generate a packmaker.lock file.

    positional arguments:
      packdef     modpack definition file

    optional arguments:
      -h, --help  show this help message and exit

Describe cmd here

Optional command flags:


.. _modsinfo;

modsinfo
````````
::

   packmaker modsinfo [-h] [-f {csv,json,table,value,yaml}] [-c COLUMNS]
                           [--max-width <integer>] [--fit-width]
                           [--print-empty]
                           [--quote {all,minimal,none,nonnumeric}] [--noindent]
                           [--sort-column SORT_COLUMN]
                           [lockfile [lockfile ...]]

   Display information about the mods in the modpack.

   positional arguments:
     lockfile              modpack lock file

   optional arguments:
     -h, --help            show this help message and exit

   output formatter:
     output formatter options

     -f {csv,json,table,value,yaml}, --format {csv,json,table,value,yaml}
                           the output format, defaults to table
     -c COLUMNS, --column COLUMNS
                           specify the column(s) to include, can be repeated
     --sort-column SORT_COLUMN
                           specify the column(s) to sort the data (columns
                           specified first have a priority, non-existing columns
                           are ignored), can be repeated

   Table formatter:
     --max-width <integer>
                           maximum display width, <1 to disable. You cal also use
                           the MAX_DISPLAY_WIDTH environment variable, but the
                           parameter takes precedence.
     --fit-width           Fit the table to the display width. Implied if --max-
                           width greater than 0. Set the environment variable
                           FIT_WIDTH=1 to always enable
     --print-empty         Print empty table iof there is no data to show.

   CSV formatter:
     --quote {all,minimal,none,nonnumeric}
                           when to include quotes, default to nonnumeric

   json formatter:
     --noindent            whether to disable indenting the JSON


.. _resourcepacksinfo;

resourcepacksinfo
`````````````````
::

    packmaker resourcepacksinfo [-h] [-f {csv,json,table,value,yaml}]
                                       [-c COLUMNS]
                                       [--max-width <integer>]
                                       [--fit-width]
                                       [--print-empty]
                                       [--quote {all,minimal,none,nonnumeric}]
                                       [--noindent]
                                       [--sort-column SORT_COLUMN]
                                       [lockfile [lockfile ...]]

    Display information about the resourcepacks in the modpack.

    positional arguments:
      lockfile              modpack lock file

    optional arguments:
      -h, --help            show this help message and exit

    output formatter:
      output formatter options

      -f {csv,json,table,value,yaml}, --format {csv,json,table,value,yaml}
                            the output format, defaults to table
      -c COLUMNS, --column COLUMNS
                            specify the column(s) to include, can be repeated
      --sort-column SORT_COLUMN
                            specify the column(s) to sort the data (columns specified first have a priority, non-existing columns are ignored), can be repeated

    Table formatter:
      --max-width <integer>
                            maximum display width, <1 to disable. You cal also use the MAX_DISPLAY_WIDTH environment variable, but the parameter takes precedence.
      --fit-width           Fit the table to the display width. Implied if --max-width greater than 0. Set the environment variable FIT_WIDTH=1 to always enable
      --print-empty         Print empty table iof there is no data to show.

    CSV formatter:
      --quote {all,minimal,none,nonnumeric}
                            when to include quotes, default to nonnumeric

    json formatter:
      --noindent            whether to disable indenting the JSON

Describe cmd here

Optional command flags:


.. _search-cmd:

search
``````
::

    packmaker search [-h] [-f {csv,json,table,value,yaml}] [-c COLUMNS]
                            [--max-width <integer>] [--fit-width] [--print-empty]
                            [--quote {all,minimal,none,nonnumeric}] [--noindent]
                            [--sort-column SORT_COLUMN]
                            searchstring [searchstring ...]

    Search curseforge for mods.

    positional arguments:
      searchstring          search string

    optional arguments:
      -h, --help            show this help message and exit

    output formatter:
      output formatter options

      -f {csv,json,table,value,yaml}, --format {csv,json,table,value,yaml}
                            the output format, defaults to table
      -c COLUMNS, --column COLUMNS
                            specify the column(s) to include, can be repeated
      --sort-column SORT_COLUMN
                            specify the column(s) to sort the data (columns
                            specified first have a priority, non-existing columns
                            are ignored), can be repeated

    Table formatter:
      --max-width <integer>
                            maximum display width, <1 to disable. You cal also use
                            the MAX_DISPLAY_WIDTH environment variable, but the
                            parameter takes precedence.
      --fit-width           Fit the table to the display width. Implied if --max-
                            width greater than 0. Set the environment variable
                            FIT_WIDTH=1 to always enable
      --print-empty         Print empty table iof there is no data to show.

    CSV formatter:
      --quote {all,minimal,none,nonnumeric}
                            when to include quotes, default to nonnumeric

    json formatter:
      --noindent            whether to disable indenting the JSON


Describe cmd here

Optional command flags:


.. _updatedb-cmd:

updatedb
````````
::

    packmaker updatedb [-h] [--ignore-mods] [--ignore-resourcepacks]

    Download and compile a new mods database from curseforge.

    optional arguments:
      -h, --help            show this help message and exit
      --ignore-mods         Do not scan for mods when updating the curseforge db
      --ignore-resourcepacks
                            Do not scan for resourcepacks when updating the curseforge db


Describe cmd here

Optional command flags:


.. _help-cmd:

help
````
::

    packmaker help [-h] cmd

    Print detailed help for another command

    positional arguments:
      cmd         name of the command

    optional arguments:
      -h, --help  show this help message and exit

Describe cmd here

Optional command flags:


Global Flags
------------

Packmaker has a few "global" flags that affect all commands. These must appear
between the executable name (``packmaker``) and the command---for example, ``packmaker -v
search ...``.


